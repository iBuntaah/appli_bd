<?php

require_once 'src/utils/autoload.php';
require_once 'vendor/autoload.php';
Autoload::register();

use Illuminate\Database\Capsule\Manager as DB;
use Contoleur\ControleurTD5 as Controleur;

// Connexion

$tab = parse_ini_file('src/conf/conf.ini');
$db = new DB();
$db->addConnection($tab);
$db->setAsGlobal();
$db->bootEloquent();

$slim = new \Slim\Slim();
$slim->response->headers->set('Content-Type', 'application/json');


$slim->get('/', function(){
    echo "DABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!";
});

$slim->get('/api/games/:id', function($id){
    $con=new Controleur();
    $content=$con->getJeu($id);
    echo json_encode($content,JSON_PRETTY_PRINT);
});

$slim->get('/api/games', function(){
    if(array_key_exists('page',$_GET)){
        $page=$_GET['page'];
        $con=new Controleur();
        $content=$con->paginationNew($page);
        $page--;
        $content[]=['links'=>array('prev' => 'href=index.php/api/games?page='.$page)];
        $page=$page+2;
        $content[]=['links'=>array('prev' => 'href=index.php/api/games?page='.$page)];
    }else {
        $con = new Controleur();
        $content = $con->getGames();
    }
    echo json_encode($content, JSON_PRETTY_PRINT);
});

$slim->get('/api/games/:id/comments', function($id){
    $con=new Controleur();
    $content = $con->afficherCom($id);
    echo json_encode($content, JSON_PRETTY_PRINT);
});



$slim->run();