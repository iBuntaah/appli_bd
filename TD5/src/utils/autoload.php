<?php

/**
 * Created by PhpStorm.
 * User: ToLgA
 * Date: 31/10/2016
 * Time: 10:54
 */
class autoload
{

    public function __construct($lien) {
        require_once('src/' . $lien . '.php');

    }

    /**
     * Enregistre notre autoloader
     */
    static function register(){
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }



    /**
     * Inclue le fichier correspondant à notre classe
     * @param $class string Le nom de la classe à charger
     */
    static function autoload($class){
        require_once 'src/' . $class . '.php';
    }


}