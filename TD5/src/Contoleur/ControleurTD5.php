<?php

namespace Contoleur;

require_once 'src/models/Game.php';
require_once 'src/models/Commentaire.php';
use src\models\Commentaire;
use src\models\Utilisateur;
use src\models\Game;

class ControleurTD5{

    public function getJeu($id){
        $res=Game::where('id','=',$id)->get();
        return $res;
    }

    public function getGames(){
        $res=Game::take(200)->get();
        return $res;
    }

    public function pagination($page){
        $res=Game::skip(200*$page-200)->take(200)->get();
        return $res;
    }

    public function paginationNew($page){
        $requete=Game::skip(200*$page-200)->take(200)->get();
        foreach ($requete as $key => $value){

            $res['games'][]=[$value,'links'=>array('self'=>'href=index.php/api/game/'.$value->id,'comments'=>'href=index.php/api/game/'.$value->id.'/comments'
            ,'characters'=>'href=index.php/api/game/'.$value->id.'/characters')];
        }
        return $res;
    }

    public function afficherCom($id){
        $requete[]=Commentaire::where('id_game','=',$id)->select('id','titre','contenu','created_at')->get();
        return $requete;

    }

}