<?php

namespace src\models;

class Utilisateur extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'utilisateur';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function Commentaire(){
        return $this->hasMany('\src\models\Utilisateur','id');
    }

}

