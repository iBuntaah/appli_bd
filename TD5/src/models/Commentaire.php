<?php

namespace src\models;

class Commentaire extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'commentaire';
    protected $primaryKey = 'id';
    public $timestamps = true;


    public function Utilisateur(){
        return $this->belongsTo('\src\models\Utilisateur','id');
    }

}