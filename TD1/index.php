<?php

require_once 'vendor/autoload.php';
require_once 'src/Contoleur/ControleurAffichage.php';

use Illuminate\Database\Capsule\Manager as DB;
use Controleur\ControleurAffichage as affich;




// Connexion

    $tab = parse_ini_file('src/conf/conf.ini');
    $db = new DB();
    $db->addConnection($tab);
    $db->setAsGlobal();
    $db->bootEloquent();

$slim = new \Slim\Slim();

$slim->get('/', function(){
    $aff= new affich();
    $content=$aff->index();
    echo $content;

});


$slim->get('/Mario', function(){
    $aff= new affich();
    $content=$aff->chercheMario();
    echo $content;

});

$slim->get('/Japon', function(){
    $aff= new affich();
    $content=$aff->compagniesJap();
    echo $content;

});

$slim->get('/Platform', function(){
    $aff= new affich();
    $content=$aff->platform();
    echo $content;

});

$slim->get('/Jeux', function(){
    $aff= new affich();
    $content=$aff->listJeux();
    echo $content;

});

$slim->get('/ListJeux/:page', function($page){
    $aff= new affich();
    $content=$aff->afficherToutLesJeux($page);
    echo $content;

});

$slim->run();