<?php
/**
 * Created by PhpStorm.
 * User: Myriam
 * Date: 06/03/2017
 * Time: 08:57
 */
namespace src\models;

class Photo extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'photo';
    protected $primaryKey = 'id';
    public $timestamps = false;

}