<?php
/**
 * Created by PhpStorm.
 * User: Myriam
 * Date: 06/03/2017
 * Time: 08:56
 */
namespace src\models;

class Annonce extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'annonce';
    protected $primaryKey = 'id';
    public $timestamps = false;

}