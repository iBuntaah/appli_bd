<?php
namespace Controleur;
require_once 'src/models/Company.php';
require_once 'src/models/Game.php';
require_once 'src/models/Platform.php';

use src\models\Game as Game;
use models\Company as Company;
use src\models\Platform as Platform;

class ControleurAffichage {

    public function chercheMario(){

        $p= Game::where('name', 'LIKE', '%mario%')->get();
        $content='';
        foreach ($p as $value){
            $content.= $value->name.'<br>';
        }
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function compagniesJap(){
        $p= Company::where('location_country', '=', 'Japan')->get();
        $content='';
        foreach ($p as $value){
            $content.= $value->name.'<br>';
        }
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function platform(){
        $p= Platform::where('install_base', '>=', '10000000')->get();
        $content='';
        foreach ($p as $value){
            $content.= $value->name.'<br>';
        }
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function listJeux(){
        $p= Game::where('id', '>=', '21173')->take(442)->get();
        $content='';
        foreach ($p as $value){
            $content.= $value->id.' : '.$value->name.'<br>';
        }
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function afficherToutLesJeux($page){

        $s=$page*500-500;
        $p= Game::take(500)->skip($s)->get();
        $content='';
        if($p->count()>0) {
            foreach ($p as $value) {
                $content .= $value->id . ' : ' . $value->name . ' <br>'.$value->deck. '<br>';
            }
            $page++;
            return $this->render($content) . '<br><a href="../ListJeux/' . $page . '">Suivant</a><br><a href="../../">Retour</a>';
        }else{
            return $this->render($content) . '<br><a href="../../">Retour</a>';
        }
    }

    public function index(){
        $content='    <a href="index.php/Mario">lister les jeux dont le nom contient \'Mario\'</a><br>
    <a href="index.php/Japon">lister les compagnies installées au Japon</a><br>
      <a href="index.php/Platform">lister les plateformes dont la base installée est >= 10 000 000</a><br>
      <a href="index.php/Jeux">lister 442 jeux à partir du 21173ème</a><br>
      <a href="index.php/ListJeux/1">lister les jeux, afficher leur nom et deck, en paginant (taille des pages : 500)</a><br>
    
';
        return $this->render($content);
    }

    public function render($content){
        $co='<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TD1 APP_BDD</title>
    <link rel="stylesheet" type="" href="">
  </head>
  <body>'.$content.'
  </body>
</html>
';
        return $co;
    }
}