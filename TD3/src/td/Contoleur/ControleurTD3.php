<?php
/**
 * Created by PhpStorm.
 * User: Myriam
 * Date: 06/03/2017
 * Time: 09:42
 */
namespace Controleur;
require_once 'src/td/models/Company.php';
require_once 'src/td/models/Game.php';
require_once 'src/td/models/Platform.php';
require_once 'src/td/models/Annonce.php';
require_once 'src/td/models/CategorieAnnonce.php';
require_once 'src/td/models/Categorie.php';
require_once 'src/td/models/Photo.php';
require_once 'src/td/models/Character.php';
require_once 'src/td/models/GameRate.php';
require_once 'src/td/models/RatingBoard.php';
require_once 'src/td/models/Genre.php';


use src\td\models\Game as Game;
use td\models\Company as Company;
use src\td\models\Platform as Platform;
use src\td\models\CategorieAnnonce as CategorieAnnonce;
use src\td\models\Categorie as Categorie;
use src\td\models\Photo as Photo;
use src\td\models\Annonce as Annonce;
use src\td\models\Character as Character;
use src\td\models\Genre as Genre;

class ControleurTD3{


    public function prepa(){
        $time_start = microtime(true);

        for($i=0; $i<=20; $i++){
            echo $i."<br>";
        }

        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Le temps d'exécution pour compter jusqu'a 20 est de ".$time." secondes";
    }

    public function index(){
        $content='<h1>Préparation td3</h1><br>    
        <a href="index.php/prepa">1. écrivez le code pour mesurer le temps d\'exécution d\'une séquence d\'instructions PHP.</a><br>
       
      
    
<hr><br><h1>TD3 Partie 1</h1>';

        $content.='
</a><br><a href="index.php/listerJeux">lister l\'ensemble des jeux<a><br>
</a><br><a href="index.php/listerJeuxMario">lister les jeux dont le nom contient \'Mario\'<a><br>
</a><br><a href="index.php/affPersJeuMario">afficher les personnages des jeux dont le nom débute par \'Mario\'<a><br>
</a><br><a href="index.php/affMarioPlus3">lister les jeux dont le nom contient \'Mario\'<a><br>
<hr><br><h1>TD3 Partie 2</h1>
</a><br><a href="index.php/IndexReponse">Réponse Partie 1 question 2<a><br>
</a><br><a href="index.php/JeuContientValeur1">lister les jeux dont le nom contient valeur1<a><br>
</a><br><a href="index.php/JeuContientValeur2">lister les jeux dont le nom contient valeur2<a><br>
</a><br><a href="index.php/JeuContientValeur3">lister les jeux dont le nom contient valeur3 <a><br>';

        return $this->render($content);
    }




    public function listerJeux(){
        $comp='';
        $time_start = microtime(true);
        $g=Game::select('name')->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Le temps d'exécution de la requete est de ".$time." secondes"."<br><br>";

        foreach ($g as $value){
            $comp.=$value->name.'<br>';
        }

        return $this->render($comp);
    }

    public function listerJeuxMario(){
        $comp='';
        $time_start = microtime(true);
        $g=Game::select('name')->where('name','LIKE','%mario%')->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Le temps d'exécution de la requete est de ".$time." secondes"."<br><br>";

        foreach ($g as $value){
            $comp.=$value->name.'<br>';
        }

        return $this->render($comp);
    }

    public function affPersJeuMario(){
        $Perso='';
        $time_start = microtime(true);
        $jeu= Game::where('name','LIKE','Mario%')->get();
        foreach ($jeu as $value){
            $Perso.=$value->name.' : '.'<br>';
            foreach ($value->AssocChar as $v) {
                $Perso .= $v->name.'<br>';

            }
            $Perso.='<hr>';
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Le temps d'exécution de la requete est de ".$time." secondes"."<br><br>";
        $content='<p>'.$Perso.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function affPersJeuMarioPlus3(){
        $Perso='';
        $time_start = microtime(true);
        $g=Game::where('name','LIKE','Mario%')
            ->whereHas('gameToRate',function ($g){
                $g->where('name','LIKE','%3+%');
            })->get();

        foreach ($g as $value){
            $Perso.=$value->name.' : '.'<br>';

        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Le temps d'exécution de la requete est de ".$time." secondes"."<br><br>";
        $content='<p>'.$Perso.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function JeuContientValeur1(){
        $comp='';
        $time_start = microtime(true);
        $g=Game::select('name')->where('name','LIKE','%Vs%')->get();
        $time=microtime(true)-$time_start;
        echo "Le temps d'exécution de la requete est de ".$time." secondes"."<br><br>";
        foreach ($g as $value){
            $comp.=$value->name.'<br>';
        }
        return $this->render($comp);

    }
    public function JeuContientValeur2(){
        $comp='';
        $time_start = microtime(true);
        $g=Game::select('name')->where('name','LIKE','%World%')->get();
        $time=microtime(true)-$time_start;
        echo "Le temps d'exécution de la requete est de ".$time." secondes"."<br><br>";
        foreach ($g as $value){
            $comp.=$value->name.'<br>';
        }
        return $this->render($comp);

    }
    public function JeuContientValeur3(){
        $comp='';
        $time_start = microtime(true);
        $g=Game::select('name')->where('name','LIKE','%Game%')->get();
        $time=microtime(true)-$time_start;
        echo "Le temps d'exécution de la requete est de ".$time." secondes"."<br><br>";
        foreach ($g as $value){
            $comp.=$value->name.'<br>';
        }
        return $this->render($comp);

    }
    public function JeuIndexValeur1(){

        $comp= "Le temps d'exécution de la requete est d'envirion 1 a 10 secondes , Mais qd la table subit un indexage sur la colone nom le temps d'éxecution est de "."<br><br>";

        return $this->render($comp);

    }

    public function render($content){
        $co='<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TD3 APP_BDD</title>
    <link rel="stylesheet" type="" href="">
  </head>
  <body>'.$content.'
  </body>
</html>
';
        return $co;
    }

}