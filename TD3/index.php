<?php

require_once 'vendor/autoload.php';
require_once 'src/td/Contoleur/ControleurTD3.php';
use Illuminate\Database\Capsule\Manager as DB;
use Controleur\ControleurTD3 as Control;


// Connexion

    $tab = parse_ini_file('src/conf/conf.ini');
    $db = new DB();
    $db->addConnection($tab);
    $db->setAsGlobal();
    $db->bootEloquent();

$slim = new \Slim\Slim();



$slim->get('/', function(){
    $aff= new Control();
    $content=$aff->index();
    echo $content;

});



$slim->get('/prepa', function(){
    $aff= new Control();
    $content=$aff->prepa();
    echo $content;

});

$slim->get('/listerJeux', function(){
    $aff= new Control();
    $content=$aff->listerJeux();
    echo $content;

});


$slim->get('/listerJeuxMario', function(){
    $aff= new Control();
    $content=$aff->listerJeuxMario();
    echo $content;

});

$slim->get('/affPersJeuMario', function(){
    $aff= new Control();
    $content=$aff->affPersJeuMario();
    echo $content;

});

$slim->get('/JeuContientValeur1', function(){
    $aff= new Control();
    $content=$aff->JeuContientValeur1();
    echo $content;

});

$slim->get('/JeuContientValeur2', function(){
    $aff= new Control();
    $content=$aff->JeuContientValeur2();
    echo $content;

});
$slim->get('/JeuContientValeur3', function(){
    $aff= new Control();
    $content=$aff->JeuContientValeur3();
    echo $content;

});

$slim->get('/IndexReponse', function(){
    $aff= new Control();
    $content=$aff->JeuIndexValeur1();
    echo $content;

});

$slim->get('/affMarioPlus3', function(){
    $aff= new Control();
    $content=$aff->affPersJeuMarioPlus3();
    echo $content;
});


$slim->run();


