<?php

namespace src\models;

class Commentaire extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'commentaire';
    protected $primaryKey = 'id';
    public $timestamps = true;


    public function Utilisateur(){
        return $this->belongsTo('\src\models\Utilisateur','id');
    }

    public function genreToGame(){
        return $this->belongsToMany('\src\models\Game','game2genre','genre_id','game_id');
    }

    public function Game(){
        return $this->belongsToMany('\src\models\Game','game','id');
    }

}