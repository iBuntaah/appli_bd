<?php

namespace src\models;

class Utilisateur extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'utilisateur';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function Commentaire(){
        return $this->hasMany('\src\models\Commentaire','id_user');
    }

    public function userToCom(){
        return $this->belongsToMany('\src\models\Commentaire','user_com','id_user','id_com');
    }

}

