<?php

require_once 'vendor/autoload.php';
require_once 'src/Contoleur/ControleurTD2.php';
use Illuminate\Database\Capsule\Manager as DB;
use Controleur\ControleurTD2 as Control;


// Connexion

    $tab = parse_ini_file('src/conf/conf.ini');
    $db = new DB();
    $db->addConnection($tab);
    $db->setAsGlobal();
    $db->bootEloquent();

$slim = new \Slim\Slim();

$slim->get('/22', function(){
    $aff= new Control();
    $content=$aff->photo22();
    echo $content;

});
$slim->get('/Annonce_photo', function(){
    $aff= new Control();
    $content=$aff->annonce_Photo();
    echo $content;

});

$slim->get('/', function(){
    $aff= new Control();
    $content=$aff->index();
    echo $content;

});

$slim->get('/photo22taille', function(){
    $aff= new Control();
    $content=$aff->photo22taille();
    echo $content;

});

$slim->get('/plus3photo', function (){
   $aff=new Control();
   $content=$aff->plus3photos();
   echo $content;
});

$slim->get('/Adannoncecat', function (){
    $aff=new Control();
    $content=$aff->addAnnonceCate();
    echo $content;
});


$slim->get('/Ajout_annonce_photo', function(){
    $aff= new Control();
    $content=$aff->addPhotoAnnonce22();
    echo $content;

});

$slim->get('/Ajout_annonce_categorie', function(){
    $aff= new Control();
    $content=$aff->addAnnonceCate();
    echo $content;

});
$slim->get('/Aff_Perso_Jeu12342', function(){
    $aff= new Control();
    $content=$aff->affPersJeu12342();
    echo $content;

});
$slim->get('/Aff_Perso_JeuMario', function(){
    $aff= new Control();
    $content=$aff->affPersJeuMario();
    echo $content;

});

$slim->get('/jeu12342', function(){
    $aff= new Control();
    $content=$aff->affPersJeu12342();
    echo $content;

});

$slim->get('/jeuMario', function(){
    $aff= new Control();
    $content=$aff->affPersJeuMario();
    echo $content;

});

$slim->get('/jeuSony', function(){
    $aff= new Control();
    $content=$aff->affPersJeuSony();
    echo $content;

});

$slim->get('/RatingBoardMario', function() {
    $aff = new Control();
    $content = $aff->affGameRateMario();
    echo $content;
});

$slim->get('/jeuMarioSup3', function(){
    $aff= new Control();
    $content=$aff->affPersSup3Mario();

    echo $content;

});

$slim->get('/jeuMarioPlustrois', function(){
    $aff= new Control();
    $content=$aff->affGameRateMarioplustrois();

    echo $content;

});

$slim->get('/jeuMarioPlustroisInc', function(){
    $aff= new Control();
    $content=$aff->affGameRateMarioInc();
    echo $content;

});

$slim->get('/creerGenre', function(){
    $aff= new Control();
    $content=$aff->creerGenre();
    echo $content;

});

$slim->run();