<?php
/**
 * Created by PhpStorm.
 * User: Myriam
 * Date: 06/03/2017
 * Time: 08:56
 */
namespace src\models;

class CategorieAnnonce extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'categorieAnnonce';
    protected $primaryKey = 'idCat,idAnnonce';
    public $timestamps = false;


}