<?php

namespace models;

class Company extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'company';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function comToGame(){
        return $this->belongsToMany('\src\models\Game','game_developers','comp_id','game_id');
    }

    public function comToGamePubl(){
        return $this->belongsToMany('\src\models\Game','game_publishers','comp_id','game_id');
    }

}