<?php

namespace src\models;

class Genre extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'genre';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function genreToGame(){
        return $this->belongsToMany('\src\models\Game','game2genre','genre_id','game_id');
    }


}