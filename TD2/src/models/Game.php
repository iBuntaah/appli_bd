<?php

namespace src\models;

class Game extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function AssocChar(){
        return $this->belongsToMany('\src\models\Character','game2character','game_id','character_id');
    }

    public function gameToRate() {
        return $this->belongsToMany('\src\models\GameRate', 'game2rating', 'game_id', 'rating_id');
    }

    public function gameToPublisher() {
        return $this->belongsToMany('\src\models\Company', 'game_publishers', 'game_id', 'comp_id');
    }
}