<?php
/**
 * Created by PhpStorm.
 * User: Myriam
 * Date: 06/03/2017
 * Time: 08:56
 */
namespace src\models;

class Character extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'character';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function AssocGame(){
        return $this->belongsToMany('\src\models\Game','game2character','character_id','game_id');
    }

}