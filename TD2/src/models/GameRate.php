<?php

namespace src\models;

class GameRate extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'game_rating';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function gamerateToRateboard(){
        return $this->belongsTo('\src\models\RatingBoard','rating_board_id');
    }


    public function gameRateToGame(){
        return $this->belongsToMany('\src\models\Game','game2character','character_id','game_id');
    }
}