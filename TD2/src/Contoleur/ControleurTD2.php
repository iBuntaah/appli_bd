<?php
/**
 * Created by PhpStorm.
 * User: Myriam
 * Date: 06/03/2017
 * Time: 09:42
 */
namespace Controleur;
require_once 'src/models/Company.php';
require_once 'src/models/Game.php';
require_once 'src/models/Platform.php';
require_once 'src/models/Annonce.php';
require_once 'src/models/CategorieAnnonce.php';
require_once 'src/models/Categorie.php';
require_once 'src/models/Photo.php';
require_once 'src/models/character.php';
require_once 'src/models/GameRate.php';
require_once 'src/models/RatingBoard.php';
require_once 'src/models/Genre.php';

use src\models\Game as Game;
use models\Company as Company;
use src\models\Platform as Platform;
use src\models\CategorieAnnonce as CategorieAnnonce;
use src\models\Categorie as Categorie;
use src\models\Photo as Photo;
use src\models\Annonce as Annonce;
use src\models\Character as Character;
use src\models\Genre as Genre;

class ControleurTD2{

    public function photo22(){
        $content='';
        $p22='';
        $photos= Photo::where('idAnnonce','=',22)->get();
        foreach ($photos as $photo){
            $p22.=$photo.'<br>';
        }
        $content='<p>'.$p22.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function photo22taille(){
        $content='';
        $photos=Photo::where('idAnnonce','=',22)->where('taille_octet','>',100000)->get();
        foreach ($photos as $photo){
            $content.=$photo.'<br>';
        }

        return $this->render($content).'<br><a href="../">Retour</a>';
    }

     public function plus3photos(){
        $content='';
        $p22='';
        $photos= Annonce::has('Photo','>=',3)->get();
         foreach ($photos as $photo){
            $p22.=$photo.'<br>';
        }
        $content='<p>'.$p22.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }


    public function index(){
        $content='<h1>Préparation td2</h1><br>    <a href="index.php/22">les photos de l\'annonce 22</a><br>
    <a href="index.php/photo22taille">les photos de l\'annonce 22 dont la taille en octets est > 100000</a><br>
      <a href="index.php/plus3photo">les annonces possédant plus de 3 photos</a><br>
      <a href="index.php/Annonce_photo">les annonces possédant des photos dont la taille est > 100000</a><br>
      <a href="index.php/Ajout_annonce_photo">ajouter une photo à l\'annonce 22</a><br>
      <a href="index.php/Adannoncecat">ajouter l\'annonce 22 aux catégories 42 et 73</a><br>
      
    
<hr><br><h1>TD2</h1>';

        $content.='
      <a href="index.php/jeu12342">afficher (name , deck) les personnages du jeu 12342</a><br>
      <a href="index.php/jeuMario">les personnages des jeux dont le nom (du jeu) débute par \'Mario\'</a><br>
<
      <a href="index.php/jeuSony">les jeux développés par une compagnie dont le nom contient \'Sony\'</a><br>
      <a href="index.php/RatingBoardMario">le rating initial (indiquer le rating board) des jeux dont le nom contient Mario</a><br>
      <a href="index.php/jeuMarioSup3">les jeux dont le nom débute par Mario et ayant plus de 3 personnages</a><br>

      <a href="index.php/jeuMarioPlustrois">les jeux dont le nom débute par Mario et dont le rating initial contient "3+"</a><br>
      <a href="index.php/jeuMarioPlustroisInc">les jeux dont le nom débute par Mario, publiés par une compagnie dont le nom contient
"Inc." et dont le rating initial contient "3+"</a><br>
      <a href="#">les jeux dont le nom débute Mario, publiés par une compagnie dont le nom contient "Inc",
dont le rating initial contient "3+" et ayant reçu un avis de la part du rating board nommé
"CERO"
</a><br>
      <a href="index.php/creerGenre">ajouter un nouveau genre de jeu, et l\'associer aux jeux 12, 56, 12, 345</a><br>';
        return $this->render($content);
    }



    public function annonce_Photo(){
        $content='';
        $annonce_photo='';
        $photos=Photo::where('taille_octet','>',100000)->get();
        foreach ($photos as $photo){
            $annonce_photo.= $photo->annonce()->get();
        }
        $content='<p>'.$annonce_photo.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function addPhotoAnnonce22()
    {
        $Photo22= new Photo();
        $Photo22->taille_octet=150000;
        $Photo22->idAnnonce=22;
        $Photo22->save();
        $content="Une nouvelle photo a été ajouté à l'annonce 22";
        return $this->render($content).'<br><a href="../">Retour</a>';

    }
    public function addAnnonceCate(){
        $annonceCat42=new CategorieAnnonce();
        $annonceCat42->idCat=42;
        $annonceCat42->idAnnonce=22;
        $annonceCat42->save();

        $annonceCat73=new CategorieAnnonce();
        $annonceCat73->idCat=73;
        $annonceCat73->idAnnonce=22;
        $annonceCat73->save();

        $content="Une nouvelle annonce a été ajouté à la categorie 42 et 73";
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function affPersJeu12342(){
        $Perso='';
        $jeu= Game::where('id','=',12342)->get();
        foreach ($jeu as $value){
            foreach ($value->AssocChar as $v) {
                $Perso .= $v->name. '<br>';
                $Perso .= $v->deck. '<br>';
                $Perso.='<hr>';
                //$Perso.=Character::where('id','=',$value->character_id)->id;
            }
        }
        $content='<p>'.$Perso.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }
    public function affPersJeuMario(){
        $Perso='';
        $jeu= Game::where('name','LIKE','Mario%')->get();
        foreach ($jeu as $value){
            $Perso.=$value->name.' : '.'<br>';
            foreach ($value->AssocChar as $v) {
                $Perso .= $v->name.'<br>';


            }
            $Perso.='<hr>';
        }
        $content='<p>'.$Perso.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }


    public function affPersJeuSony(){
        $comp='';
        $jeu= Company::where('name','LIKE','%sony%')->get();
        foreach ($jeu as $value){
            $comp.=$value->name.' : '.'<br>';
            foreach ($value->comToGame as $v) {
                $comp .= $v->name.'<br>';
            }
            $comp.='<hr>';
        }
        $content='<p>'.$comp.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function affGameRateMario(){
        $comp='';
        $jeu= Game::where('name','LIKE','%Mario%')->get();
        foreach ($jeu as $value){
            $comp.=$value->name.' : '.'<br>';
            foreach ($value->gameToRate as $v) {
                $v->belongsTo('\src\models\RatingBoard','rating_board_id');
                $comp .= substr($v->name,0,4).'<br>';
            }
            $comp.='<hr>';
        }
        $content='<p>'.$comp.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }


    public function affPersSup3Mario(){
        $Perso='';
        $jeu= Game::where('name','LIKE','Mario%')->get();
        foreach ($jeu as $value){

            if(sizeof($value->AssocChar)>=3) {
                $Perso.=$value->name.'<br>';
                foreach ($value->AssocChar as $v) {

                    $Perso .= $v->name . '<br>';

                }
                $Perso.='<hr>';

            }
        }
        $content='<p>'.$Perso.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function affGameRateMarioplustrois(){
        $comp='';
        $jeu= Game::where('name','LIKE','Mario%')->get();
        foreach ($jeu as $value){
            $i=0;
            foreach ($value->gameToRate as $v) {
                if (strpos($v->name,'3+')== true) {
                    if($i==0){
                        $comp.=$value->name.' : '.'<br>';
                        $i++;
                    }
                    $comp .= $v->name . '<br>';
                }
            }
            if($i==1){

                $comp.='<hr>';
            }
        }
        $content='<p>'.$comp.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }

    public function affGameRateMarioInc(){
            $comp='';
            $jeu= Company::where('name','LIKE','%inc%')->get();
            foreach ($jeu as $value){
            $i=0;
                foreach ($value->comToGamePubl as $game) {
                   if(strpos($game->name,'Mario')) {
                       foreach ($game->gameToRate as $v) {
                           if (strpos($v->name, '3+') == true) {
                               $comp .= $value->name . ' : ' . '<br>';
                               $comp .= $game->name . '<br>';
                               $comp .= $v->name . '<br>';
                               $i++;
                           }
                       }
                   }
                }
                if($i!=0){

                    $comp.='<hr>';
                }
            }
            $content='<p>'.$comp.'</p>';
            return $this->render($content).'<br><a href="../">Retour</a>';
    }


    public function creerGenre(){
        $g = new Genre();
        $g->name='nouveau genre';
        $g->save();
        $g->genreToGame()->attach([12,56,345]);

        $content='<p>'.'ok'.'</p>';
        return $this->render($content).'<br><a href="../">Retour</a>';
    }


        public function render($content){
        $co='<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TD1 APP_BDD</title>
    <link rel="stylesheet" type="" href="">
  </head>
  <body>'.$content.'
  </body>
</html>
';
        return $co;
    }

}